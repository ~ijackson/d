#

all:		words.final

o= >$@.tmp && mv -f $@.tmp $@

words.final:	words.interim forbidden-words Makefile
		grep -vxFf forbidden-words $< $o

words.interim:	massage-lemmas lemma.al
		./$^ $o
